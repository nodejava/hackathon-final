/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codewarrriors.hackathon.repository;

import com.codewarrriors.hackathon.model.ProdItem;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Hareendran
 */
@Repository
public interface ProdItemRepository extends CrudRepository<ProdItem, Long> {
    
    
}   
