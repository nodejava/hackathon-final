/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codewarrriors.hackathon.controller;

import com.codewarrriors.hackathon.model.ProdItem;
import com.codewarrriors.hackathon.repository.ProdItemRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hareendran
 */
@RestController
public class ProdItemController {
    @Autowired
    ProdItemRepository prodItemRepository;
    @RequestMapping(value = "/api", method = RequestMethod.GET)
    public Optional<ProdItem> ProdItem(){
        System.out.println("com.codewarrriors.hackathon.controller.ProdItemController.ProdItem()");
        return prodItemRepository.findById((long)1);
       
    }
    
    
    @RequestMapping(value = "/api", method = RequestMethod.POST)
    public void ProdItem(@RequestBody ProdItem item){
    
        System.out.println("abc");
        prodItemRepository.save(item);
        
   }
    
    
    @RequestMapping(name="/hi", method = RequestMethod.GET)
    public String sayHI()
    {
        return "abcde";
    }
}
