/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Mudara Bandaranayake
 */
/* Product class to hold data of each product which runs through each step*/
public class Product {
    
    /*Attributes of each product which passes through each step*/
    private int products=0;
    private int defects=0;
    Connection conn = DBconnecter.connect();
    PreparedStatement stmt=null;
    /* Constructor of the product class which receives and set values to the attributes*/
    public Product(int Products,int Defects) {
        this.products = Products;
        this.defects = Defects;
    }
    
    public Product() {
        
    }
    /*Getters and setters to be used to extract information and set values for each product which passes each step*/
    public int getProducts() {
        return products;
    }

    public int getDefects() {
        return defects;
    }
    
    public void setProducts(int prod) {
        this.products = prod;
    }

    public void setdefectStatus(int def) {
        this.defects = def;
    }
    
    /*Returns the no.of items which passes through sensor 1 within one second*/
    public int getNumberofitemsS1(){
//        Product product=new Product();
//        int Nitems=0;
//        List<Product> productList=new ArrayList<Product>();
//        productList.add(product);
//        Nitems=productList.size();
//        return Nitems;
        int S1Prods=0;
        try {
                    stmt=conn.prepareStatement("SELECT SUM(Products) FROM sensor1");  
                    ResultSet result=stmt.executeQuery();
                    System.out.println("Got results:");
                    while (result.next()) { // process results one row at a time
                        S1Prods = result.getInt(1);
                        System.out.println("Products: " + S1Prods);
                    }
               return S1Prods;
            } 
        catch (Exception e) {
                    e.printStackTrace();
                    return -1;
            } 
    }
    
    /*Returns the no.of defect items which passes through sensor 1 within one second*/
    public int getNumberofdefectsS1(){
//        int Ndefects=0;
//        Product product=new Product();
//        List<Product> productList=new ArrayList<Product>();
//        productList.add(product);
//        for(Product prod : productList){
////            if(prod.defects=="defect"){
////                Ndefects++;
////            }
//        }
//        return Ndefects;
        int S1Defs=0;
        try {
                    
                    stmt = conn.prepareStatement("SELECT SUM(Defects) FROM sensor1");
                    ResultSet result = stmt.executeQuery();
                    System.out.println("Got results:");
                    while (result.next()) { // process results one row at a time
                        S1Defs = result.getInt(1);
                        System.out.println("Products: " + S1Defs);
                    }
               return S1Defs;
            } 
        catch (Exception e) {
                    e.printStackTrace();
                    return -1;
            } 
    }
    
    
    /*Returns the no.of items which passes through sensor 2 within one second*/
    public int getNumberofitemsS2(){
//        Product product=new Product();
//        int Nitems=0;
//        List<Product> productList=new ArrayList<Product>();
//        productList.add(product);
//        Nitems=productList.size();
//        return Nitems;
        int S2Prods=0;
        try {
                    
                    stmt = conn.prepareStatement("SELECT SUM(Products) FROM sensor2");
                    ResultSet result = stmt.executeQuery();
                    System.out.println("Got results:");
                    while (result.next()) { // process results one row at a time
                        S2Prods = result.getInt(1);
                        System.out.println("Products: " + S2Prods);
                    }
               return S2Prods;
            } 
        catch (Exception e) {
                    e.printStackTrace();
                    return -1;
            } 
    }
    
    
    /*Returns the no.of defect items which passes through sensor 2 within one second*/
    public int getNumberofdefectsS2(){
//        int Ndefects=0;
//        Product product=new Product();
//        List<Product> productList=new ArrayList<Product>();
//        productList.add(product);
//        for(Product prod : productList){
//            if(prod.defectStatus=="defect"){
//                Ndefects++;
//            }
//        }
//        return Ndefects;
          int S2Defs=0;
          try {
                   
                    stmt = conn.prepareStatement("SELECT SUM(Defects) FROM sensor2");
                    ResultSet result = stmt.executeQuery();
                    System.out.println("Got results:");
                    while (result.next()) { // process results one row at a time
                        S2Defs = result.getInt(1);
                        System.out.println("Products: " + S2Defs);
                    }
               return S2Defs;
            } 
        catch (Exception e) {
                    e.printStackTrace();
                    return -1;
            } 
}
    
    /*Returns the no.of items which passes through sensor 3 within one second*/
    public int getNumberofitemsS3(){
        int S3Prods=0;
        try {
                    
                    stmt = conn.prepareStatement("SELECT SUM(Products) FROM sensor3");
                    ResultSet result = stmt.executeQuery()    ;
                    System.out.println("Got results:");
                    while (result.next()) { // process results one row at a time
                        S3Prods = result.getInt(1);
                        System.out.println("Products: " + S3Prods);
                    }
               return S3Prods;
            } 
        catch (Exception e) {
                    e.printStackTrace();
                    return -1;
            } 
    }
    
    
    /*Returns the no.of defect items which passes through sensor 3 within one second*/
    public int getNumberofdefectsS3(){
        int S3Defs=0;
        try {
                    
                    stmt = conn.prepareStatement("SELECT SUM(Defects) FROM sensor3");
                    ResultSet result = stmt.executeQuery(
                    );
                    System.out.println("Got results:");
                    while (result.next()) { // process results one row at a time
                        S3Defs = result.getInt(1);
                        System.out.println("Products: " + S3Defs);
                    }
               return S3Defs;
            } 
        catch (Exception e) {
                    e.printStackTrace();
                    return -1;
            } 
    }
    
    /*Returns the no.of items which passes through sensor 4 within one second*/
    public int getNumberofitemsS4(){
        int S4Prods=0;
        try {
                    
                    stmt = conn.prepareStatement("SELECT SUM(Products) FROM sensor4");
                    ResultSet result = stmt.executeQuery();
                    System.out.println("Got results:");
                    while (result.next()) { // process results one row at a time
                        S4Prods = result.getInt(1);
                        System.out.println("Products: " + S4Prods);
                    }
               return S4Prods;
            } 
        catch (Exception e) {
                    e.printStackTrace();
                    return -1;
            } 
    }
    
    
    /*Returns the no.of defect items which passes through sensor 4 within one second*/
    public int getNumberofdefectsS4(){
     int S4Defs=0;
        try {
                    
                    stmt = conn.prepareStatement("SELECT SUM(Defects) FROM sensor4");
                    ResultSet result = stmt.executeQuery();
                    System.out.println("Got results:");
                    while (result.next()) { // process results one row at a time
                        S4Defs = result.getInt(1);
                        System.out.println("Products: " + S4Defs);
                    }
               return S4Defs;
            } 
        catch (Exception e) {
                    e.printStackTrace();
                    return -1;
            } 
    }
}

