<%-- 
    Document   : index
    Created on : Sep 19, 2018, 3:05:20 PM
    Author     : Mudara Bandaranayake
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%--Imported Product Class to JSp--%>
<%@ page import="Model.Product"%>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="Dashboard.js"></script>
        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
        <link rel="stylesheet" type="text/css" href="StyleSheet.css">
    </head>
    <body>
        <%--Java methods are called and return values are stored in variables--%>    
        <%
        Product product=new Product();
        int NitemsS1=product.getNumberofitemsS1();
        int NdefectsS1=product.getNumberofdefectsS1();
        int NerrPercent1=((NdefectsS1/NitemsS1)*100);
        int NitemsS2=product.getNumberofitemsS2();
        int NdefectsS2=product.getNumberofdefectsS2();
        int NerrPercent2=((NdefectsS2/NitemsS2)*100);
        int NitemsS3=product.getNumberofitemsS3();
        int NdefectsS3=product.getNumberofdefectsS3();
        int NerrPercent3=((NdefectsS3/NitemsS3)*100);
        int NitemsS4=product.getNumberofitemsS4();
        int NdefectsS4=product.getNumberofdefectsS4();
        int NerrPercent4=((NdefectsS4/NitemsS4)*100);
        %>
        <div id="wrapper">
            
            <%--The variables which store return values are stored in hidden tags to be retrieved by the Javascript--%>
            <input id='hdnNitemsS1' type='hidden' value='<%=NitemsS1%>' />
            <input id='hdnNdefectsS1' type='hidden' value='<%=NdefectsS1%>' />
            <input id='hdnNerr1' type='hidden' value='<%=NerrPercent1%>' />
            <input id='hdnNitemsS2' type='hidden' value='<%=NitemsS2%>' />
            <input id='hdnNdefectsS2' type='hidden' value='<%=NdefectsS2%>' />
            <input id='hdnNerr2' type='hidden' value='<%=NerrPercent2%>' />
            <input id='hdnNitemsS3' type='hidden' value='<%=NitemsS3%>' />
            <input id='hdnNdefectsS3' type='hidden' value='<%=NdefectsS3%>' />
            <input id='hdnNerr3' type='hidden' value='<%=NerrPercent3%>' />
            <input id='hdnNitemsS4' type='hidden' value='<%=NitemsS4%>' />
            <input id='hdnNdefectsS4' type='hidden' value='<%=NdefectsS4%>' />
            <input id='hdnNerr4' type='hidden' value='<%=NerrPercent4%>' />
            <h1>Real Time Dashboard</h1>
            <p>Welcome to the Production Line Real Time Dashboard.</p>
            <br />
            <div id="chartContainer" style="height: 300px; width: 100%;"></div>
             <div id="chartContainerSpace" style="height: 100px; width: 100%;"></div>
            <div id="chartContainer2" style="height: 300px; width: 100%;"></div>
        </div>
    </body>
</html>

