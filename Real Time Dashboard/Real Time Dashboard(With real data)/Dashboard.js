/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//When the window loads the function which shows a line chart for items passed through each step will be shown.
window.onload = function() {
    
//When the function starts errorPercentage() function is called to display a line chart which depicts no.of defect products passed through each step.
errorPercentage();

// Declaring arrays to store values to be shown on the line chart and adjusting format,title etc.
var dataPoints1 = [];
var dataPoints2 = [];
var dataPoints3 = [];
var dataPoints4 = [];

var chart = new CanvasJS.Chart("chartContainer", {
	zoomEnabled: true,
	title: {
		text: "Items produced by each stage"
	},
	axisX: {
		title: "Updates every second"
	},
	axisY:{
		includeZero: false
	}, 
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		verticalAlign: "top",
		fontSize: 22,
		fontColor: "dimGrey",
		itemclick : toggleDataSeries
	},
	data: [{ 
		type: "line",
		xValueType: "dateTime",
		yValueFormatString: "####",
		xValueFormatString: "hh:mm:ss TT",
		showInLegend: true,
		name: "Stage 1",
		dataPoints: dataPoints1
		},
		{				
			type: "line",
			xValueType: "dateTime",
			yValueFormatString: "####",
			showInLegend: true,
			name: "Stage 2" ,
			dataPoints: dataPoints2
                },
                {				
			type: "line",
			xValueType: "dateTime",
			yValueFormatString: "####",
			showInLegend: true,
			name: "Stage 3" ,
			dataPoints: dataPoints3
                },
                {				
			type: "line",
			xValueType: "dateTime",
			yValueFormatString: "####",
			showInLegend: true,
			name: "Stage 4" ,
			dataPoints: dataPoints4
                }
    ]
});

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

var updateInterval = 1000;

// initial value
var yValue1 = 0; 
var yValue2 = 0;
var yValue3 = 0; 
var yValue4 = 0;

var time = new Date;

// starting at current time
time.setHours(time.getHours());
time.setMinutes(time.getMinutes());
time.setSeconds(time.getSeconds());
time.setMilliseconds(time.getMilliseconds());

function updateChart(count) {
	count = count || 1;
	for (var i = 0; i < count; i++) {
            time.setTime(time.getTime()+ updateInterval);
              
        // Values returned by Java methods which counts No.of items per second is shown on the real time dashboard.
	yValue1 = document.getElementById('hdnNitemsS1').value;
	yValue2 = document.getElementById('hdnNitemsS2').value;
        yValue3 = document.getElementById('hdnNitemsS3').value;
        yValue4 = document.getElementById('hdnNitemsS4').value;
        
        //For Testing Purposes of the line chart.
//	yValue1 = .5 + Math.random() *(-.5-.5);
//	yValue2 = .5 + Math.random() *(-.5-.5);
//      yValue3 = .5 + Math.random() *(-.5-.5);
//      yValue4 = .5 + Math.random() *(-.5-.5);

	// pushing the new values
	dataPoints1.push({
		x: time.getTime(),
		y: yValue1
	});
	dataPoints2.push({
		x: time.getTime(),
		y: yValue2
	});
        dataPoints3.push({
		x: time.getTime(),
		y: yValue3
	});
	dataPoints4.push({
		x: time.getTime(),
		y: yValue4
	});
	}

	// updating legend text with  updated with y Value 
	chart.options.data[0].legendText = " Stage 1: " + yValue1;
	chart.options.data[1].legendText = " Stage 2: " + yValue2; 
        chart.options.data[2].legendText = " Stage 3: " + yValue3;
	chart.options.data[3].legendText = " Stage 4: " + yValue4;
	chart.render();
}
// generates first set of dataPoints 
updateChart(100);	
setInterval(function(){updateChart()}, updateInterval);
}



function errorPercentage(){
    
// Declaring arrays to store values to be shown on the line chart and adjusting format,title etc.
var dataPoints1 = [];
var dataPoints2 = [];
var dataPoints3 = [];
var dataPoints4 = [];

var chart = new CanvasJS.Chart("chartContainer2", {
	zoomEnabled: true,
	title: {
		text: "Defectssss Items produced by each stage"
	},
	axisX: {
		title: "Updates every second"
	},
	axisY:{
		includeZero: true
	}, 
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		verticalAlign: "top",
		fontSize: 22,
		fontColor: "dimGrey",
		itemclick : toggleDataSeries
	},
	data: [{ 
		type: "line",
		xValueType: "dateTime",
		yValueFormatString: "####",
		xValueFormatString: "hh:mm:ss TT",
		showInLegend: true,
		name: "Stage 1",
		dataPoints: dataPoints1
		},
		{				
			type: "line",
			xValueType: "dateTime",
			yValueFormatString: "####",
			showInLegend: true,
			name: "Stage 2" ,
			dataPoints: dataPoints2
                },
                {				
			type: "line",
			xValueType: "dateTime",
			yValueFormatString: "####",
			showInLegend: true,
			name: "Stage 3" ,
			dataPoints: dataPoints3
                },
                {				
			type: "line",
			xValueType: "dateTime",
			yValueFormatString: "####",
			showInLegend: true,
			name: "Stage 4" ,
			dataPoints: dataPoints4
                }
    ]
});

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

var updateInterval = 1000;

// initial value
var yValue1 = 0; 
var yValue2 = 0;
var yValue3 = 0; 
var yValue4 = 0;

var time = new Date;

// starting at current time
time.setHours(time.getHours());
time.setMinutes(time.getMinutes());
time.setSeconds(time.getSeconds());
time.setMilliseconds(time.getMilliseconds());

function updateChart(count) {
	count = count || 1;
	for (var i = 0; i < count; i++) {
		time.setTime(time.getTime()+updateInterval);
                
	// Values returned by Java methods which counts No.of defect items per second is shown on the real time dashboard.
        
        yValue1 = document.getElementById('hdnNdefectsS1').value;
        yValue2 = document.getElementById('hdnNdefectsS2').value;
        yValue3 = document.getElementById('hdnNdefectsS3').value;
        yValue4 = document.getElementById('hdnNdefectsS4').value;
       
        //For Testing Purposes of the line chart.
//        yValue1 = .5 + Math.random() *(-.5-.5);
//        yValue2 = .5 + Math.random() *(-.5-.5);
//        yValue3 = .5 + Math.random() *(-.5-.5);
//        yValue4 = .5 + Math.random() *(-.5-.5);
        
        
	// pushing the new values
	dataPoints1.push({
		x: time.getTime(),
		y: yValue1
	});
	dataPoints2.push({
		x: time.getTime(),
		y: yValue2
	});
        dataPoints3.push({
		x: time.getTime(),
		y: yValue3
	});
	dataPoints4.push({
		x: time.getTime(),
		y: yValue4
	});
	}

	// updating legend text with error percentage calculated in index.jsp 
	chart.options.data[0].legendText = " Stage 1: " + document.getElementById('hdnNerr1').value+"%";
	chart.options.data[1].legendText = " Stage 2: " + document.getElementById('hdnNerr2').value+"%"; 
        chart.options.data[2].legendText = " Stage 3: " + document.getElementById('hdnNerr3').value+"%";
	chart.options.data[3].legendText = " Stage 4: " + document.getElementById('hdnNerr4').value+"%";
	chart.render();
}
// generates first set of dataPoints 
updateChart(100);	
setInterval(function(){updateChart()}, updateInterval);
}

