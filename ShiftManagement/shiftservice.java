/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codewarriors.hackathon.service;

import com.codewarriors.hackathon.Repository.shiftrepository;
import com.codewarriors.hackathon.bean.Shift;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mudara Bandaranayake
 */
@Service
public abstract class shiftservice implements ishiftservice {
 
    @Autowired
    private shiftrepository repository;

    @Override
    public List<Shift> findAll() {

        List<Shift> shifts = (List<Shift>) repository.findAll();
        
        return shifts;
    }

//    @Override
//    public Shift findById(Long id) {
//
//        Shift shift = repository.findOne(id);
//        return shift;
//    }
    
}
