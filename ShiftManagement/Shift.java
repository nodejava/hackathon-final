/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codewarriors.hackathon.bean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Mudara Bandaranayake
 */

@Entity
@Table(name = "employee_shift")
public class Shift implements Serializable {
    
    private int shiftNo;
    private String employee1;
    private String employee2;
    private String employee3; 
    private String manager;
    @Id
    private Long id;
    
public Shift(int shiftNo, String employee1, String employee2, String employee3, String manager) 
    { 
        this.shiftNo = shiftNo;
        this.employee1 = employee1; 
        this.employee2 = employee2;
        this.employee3 = employee3;
        this.manager = manager;
    }

public int getShiftNo() 
{ 
    return shiftNo;
}
public void setShiftNo(int shiftNo) 
{ 
    this.shiftNo = shiftNo;
} 

 public String getEmployee1() {
        return employee1;
    }

public void setEmployee1(String employee1)
{ 
    this.employee1 = employee1;
} 

public String getEmployee2()
{
        return employee2;
}

public void setEmployee2(String employee2) 
{ 
    this.employee2 = employee2;
} 
public String getEmployee3()
{
        return employee3;
}

public void setEmployee3(String employee3) 
{ 
    this.employee3 = employee3;
} 

public String getManager()
{
        return manager;
}

public void setManager(String manager) 
{ 
    this.manager = manager;
} 
@Override
public String toString()
{
        return "[Shift No=" + shiftNo + ", Employee 1=" + employee1 + ", Employee 2=" + employee2 + ", Employee 3=" +employee3 +", Manager=" +manager+"]";
                 
} 

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
}
