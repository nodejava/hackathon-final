/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.codewarriors.hackathon.service;

import com.codewarriors.hackathon.bean.Shift;
import java.util.List;

/**
 *
 * @author Mudara Bandaranayake
 */
public interface ishiftservice {
    
      public List<Shift> findAll();
      public Shift findById(long id);
}
